**Azure subscription is forbidden now for Russia (validating debit card is impossible, tried many times with many cards), 
therefore I started `kubernetes` cluster locally with `one node` through `minikube.`**

  

Starting local kubernetes node with **minikube**:

![](screenshots/1.start_cluster.png)


Creating new namespace **confluent** and setting it to default for our Kubernetes context:

![](screenshots/2.create_namespace.png)


Building images for **azure-connector** and **kstream-app** and pushing it to **Docker Hub**:

![](screenshots/3.push_connector_image.png)

![](screenshots/4.push_app_image.png)


Showing this images in **Docker Hub**:

![](screenshots/5.docker_hub_connector_image.png)

![](screenshots/6.docker_hub_app_image.png)

Starting **confluent** components by `kubectl apply -f ./confluent-platform.yaml`.

Checking running **pods** by `kubectl get pods`:

![](screenshots/7.start_confluent.png)


Opening **Control Center** (`localhost:9021`):

![](screenshots/8.open_control_center.png)


Showing **confluent** components overview:

![](screenshots/9.local_confluent_overview.png)

Creating source topic **expedia**:

![](screenshots/10.create_source_topic.png)

Creating target topic **expedia_ext**:

![](screenshots/11.create_target_topic.png)

Adding **azure connector** on `Connect` page:

![](screenshots/12.add_azure_connector.png)

Uploading json-configuration for the connector (`JSON` file inside `connectors/` directory):

![](screenshots/13.upload_connector_configuration.png)

Showing connectors list with added **azure connector**:

![](screenshots/14.connectors_list.png)

Showing messages arriving to the source topic **expedia**:

![](screenshots/15.source_topic_messages.png)

Showing final size of the source topic **expedia**:

![](screenshots/16.source_topic_size.png)

Running **kstream-app** by `kubectl create -f kstream-app.yaml`.

Checking running **pod** by `kubectl get pods`:

![](screenshots/17.run_kstream_app.png)

Showing messages arriving to the target topic **expedia_ext**:

![](screenshots/18.target_topic_messages.png)

Showing calculated string-field `stay_category`:

![](screenshots/19.appended_field.png)

Showing final size of the target topic **expedia_ext**:

![](screenshots/20.target_topic_size.png)

Creating `KStream` from the target topic **expedia_ext** with two fields: `stay_category` and `hotel_id`:

![](screenshots/21.create_kstream.png)

Creating `KTable` by aggregating data from the `KStream`:

![](screenshots/22.create_ktable.png)

Executing **ksql-cli** by `kubectl exec --stdin --tty ksqldb-0 -- /bin/ksql`.

Showing **total number** of hotels and **number of distinct** hotels for each `category` in real time:

![](screenshots/23.visualize_stream_realtime.png)

Showing **one-moment-snapshot** of the aggregated data:

![](screenshots/24.visualize_stream_snapshot.png)



