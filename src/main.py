import datetime
from dateutil.parser import parse as parse_date, ParserError
import faust
import logging


class ExpediaRecord(faust.Record):
    id: float
    date_time: str
    site_name: int
    posa_container: int
    user_location_country: int
    user_location_region: int
    user_location_city: int
    orig_destination_distance: float
    user_id: int
    is_mobile: int
    is_package: int
    channel: int
    srch_ci: str
    srch_co: str
    srch_adults_cnt: int
    srch_children_cnt: int
    srch_rm_cnt: int
    srch_destination_id: int
    srch_destination_type_id: int
    hotel_id: int


class ExpediaExtRecord(ExpediaRecord):
    stay_category: str


logger = logging.getLogger(__name__)
app = faust.App('kafkastreams', broker='kafka://kafka:9092')
source_topic = app.topic('expedia', value_type=ExpediaRecord)
destination_topic = app.topic('expedia_ext', value_type=ExpediaExtRecord)


def get_stay_category(check_in_date: str, check_out_date: str) -> str:
    """
    - longer than 2 weeks "Long stay"
    - 11-14 days: "Standard extended stay"
    - 5-10 days: "Standard stay"
    - 1-4 days: "Short stay"
    - <= 0 days - "Erroneous data"
    """

    try:
        date_diff = parse_date(check_out_date) - parse_date(check_in_date)
        stay_days = date_diff.days
    except (ParserError, OverflowError):
        return "Erroneous data"
    if stay_days >= 15:
        return "Long stay"
    elif stay_days >= 11:
        return "Standard extended stay"
    elif stay_days >= 5:
        return "Standard stay"
    elif stay_days >= 1:
        return "Short stay"
    else:
        return "Erroneous data"


@app.agent(source_topic, sink=[destination_topic])
async def handle(messages):
    async for message in messages:
        if message is None:
            logger.info('No messages')
            continue

        data = message.asdict()
        stay_category = get_stay_category(check_in_date=message.srch_ci, check_out_date=message.srch_co)

        yield ExpediaExtRecord(**data, stay_category=stay_category)


if __name__ == '__main__':
    app.main()
